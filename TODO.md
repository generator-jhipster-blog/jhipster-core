- user role moderator

* SUBMODULE GALLERY
  - search post : image hash is found
  - save on filesystem not database
  - gallery entity
  - - refactoring "Image à la une"

- I18n move on json obj "blog"

- bug fix :
  - cat parent not reload on add
  - search with empty string
