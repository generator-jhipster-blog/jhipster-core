import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';

import { ICategory } from '../shared/model/category.model';
import { ISearch } from '../shared/model/search.model';

type EntityResponseType = HttpResponse<ICategory>;
type EntityArrayResponseType = HttpResponse<ICategory[]>;

@Injectable({ providedIn: 'root' })
export class CategoryService {
  public resourceUrl = SERVER_API_URL + 'api/blog/categories';
  public resourceUrlPublic = SERVER_API_URL + 'api/blog/public/categories';

  constructor(protected http: HttpClient) {}

  create(category: ICategory): Observable<EntityResponseType> {
    return this.http.post<ICategory>(this.resourceUrl, category, { observe: 'response' });
  }

  update(category: ICategory): Observable<EntityResponseType> {
    return this.http.put<ICategory>(this.resourceUrl, category, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<ICategory>(`${this.resourceUrlPublic}/${id}`, { observe: 'response' });
  }

  queryhierarchical(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<ICategory[]>(this.resourceUrlPublic + '/hierarchical', {
        params: options,
        observe: 'response'
      })
      .pipe(
        map((categories: EntityArrayResponseType) => {
          categories.body.forEach(cs => {
            function rename(cat: ICategory, count: number) {
              if (null !== cat.parentCategoryId) {
                count++;
                cat.name = '—'.repeat(count) + ' ' + cat.name;
              }
              cat.childrens.forEach(subCat => rename(subCat, count));
            }

            rename(cs, 0);
          });

          return categories;
        })
      );
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<ICategory[]>(this.resourceUrlPublic, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  searchFull(searchTerm: ISearch, req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.post<ICategory[]>(this.resourceUrlPublic + '/search', searchTerm, {
      params: options,
      observe: 'response'
    });
  }
}
