import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';

import { IPost } from '../shared/model/post.model';

type EntityResponseType = HttpResponse<IPost>;
type EntityArrayResponseType = HttpResponse<IPost[]>;

@Injectable({ providedIn: 'root' })
export class PostService {
  public resourceUrl = SERVER_API_URL + 'api/blog/posts';
  public resourceUrlPublic = SERVER_API_URL + 'api/blog/public/posts';

  constructor(protected http: HttpClient) {}

  create(post: IPost): Observable<EntityResponseType> {
    return this.http.post<IPost>(this.resourceUrl, post, { observe: 'response' });
  }

  update(post: IPost): Observable<EntityResponseType> {
    return this.http.put<IPost>(this.resourceUrl, post, { observe: 'response' });
  }

  find(slug: string): Observable<EntityResponseType> {
    return this.http.get<IPost>(`${this.resourceUrlPublic}/${slug}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IPost[]>(this.resourceUrlPublic, { params: options, observe: 'response' });
  }

  search(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IPost[]>(this.resourceUrlPublic + '/search', { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
