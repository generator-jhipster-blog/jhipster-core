import { Injectable } from '@angular/core';
import { Meta } from '@angular/platform-browser';

import { IPost } from '../shared/model/post.model';

@Injectable({ providedIn: 'root' })
export class SeoService {
  private static ROBOT_SEO = 'INDEX, FOLLOW';
  private static PAGE_TYPE = 'text/html';
  private static PAGE_ENCODE = 'UTF-8';
  private static FORMAT_DATE = 'YYYY-MM-DD';

  private static META_PROPERTY = 'property';
  private static META_NAME = 'name';

  constructor(protected meta: Meta) {}

  private static formatDateForSeo(date) {
    const d = new Date(date);
    let month = '' + (d.getMonth() + 1);
    let day = '' + d.getDate();
    const year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [year, month, day].join('-');
  }

  public addMetaForPost(post: IPost) {
    if (undefined !== post && null !== post) {
      const keywords = [];
      if (undefined !== post.tags && null !== post.tags) {
        post.tags.forEach(t => keywords.push(t.name));
      }
      this.addMetaTags('article', post.title, post.description, post.featuredImage, post.createdBy, post.createdDate, keywords.toString());
    }
  }

  /**
   * Add html metadata and og.
   *
   * @param type og type
   * @param title og title
   * @param description og description
   * @param image og image
   * @param author of page
   * @param date of page
   * @param keywords og page
   */
  public addMetaTags(type?: string, title?: string, description?: string, image?: any, author?: string, date?: Date, keywords?: string) {
    // static metadata
    this.meta.addTags([
      { name: 'robots', content: SeoService.ROBOT_SEO },
      { httpEquiv: 'Content-Type', content: SeoService.PAGE_TYPE },
      { charset: SeoService.PAGE_ENCODE }
    ]);

    // og metadata
    this.addTagIfExist(SeoService.META_PROPERTY, 'og:type', type);
    this.addTagIfExist(SeoService.META_PROPERTY, 'og:title', title);
    this.addTagIfExist(SeoService.META_PROPERTY, 'og:image', image);

    // dynamic metadata
    this.addTagIfExist(SeoService.META_NAME, 'author', author);
    this.addTagIfExist(SeoService.META_NAME, 'description', description);
    this.addTagIfExist(SeoService.META_NAME, 'keywords', keywords);
    this.addDateIfExist(date);
  }

  private addDateIfExist(date: Date) {
    if (undefined !== date && null !== date) {
      const propToAdd: any = {
        name: 'date',
        content: SeoService.formatDateForSeo(date),
        scheme: SeoService.FORMAT_DATE
      };
      if (null === this.meta.getTag('name="date"')) {
        this.meta.addTag(propToAdd);
      } else {
        this.meta.updateTag(propToAdd);
      }
    }
  }

  private addTagIfExist(metaType: string, metaName: string, attrValue: any) {
    if (undefined !== attrValue && null !== attrValue) {
      const propToAdd: any = {};
      propToAdd[metaType.toString()] = metaName;
      propToAdd['content'] = attrValue;
      if (null === this.meta.getTag(metaType + '="' + metaName + '"')) {
        this.meta.addTag(propToAdd);
      } else {
        this.meta.updateTag(propToAdd);
      }
    }
  }
}
