import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiResolvePagingParams } from 'ng-jhipster';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';

import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';

import { ITags, Tags } from '../../shared/model/tags.model';
import { TagsAdminComponent } from './tags-admin.component';
import { TagsDeletePopupAdminComponent } from './tags-delete-dialog-admin.component';
import { TagsService } from '../../service/tags.service';

@Injectable({ providedIn: 'root' })
export class TagsAdminResolve implements Resolve<ITags> {
  constructor(private service: TagsService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<ITags> {
    const slug = route.params['slug'];
    if (slug) {
      return this.service.find(slug).pipe(
        filter((response: HttpResponse<Tags>) => response.ok),
        map((tags: HttpResponse<Tags>) => tags.body)
      );
    }
    return of(new Tags());
  }
}

export const tagsAdminRoute: Routes = [
  {
    path: '',
    component: TagsAdminComponent,
    resolve: {
      tags: TagsAdminResolve,
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_ADMIN'],
      defaultSort: 'id,asc',
      pageTitle: 'jhipsterApp.tags.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':slug/edit',
    component: TagsAdminComponent,
    resolve: {
      tags: TagsAdminResolve,
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_ADMIN'],
      pageTitle: 'jhipsterApp.tags.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const tagsPopupAdminRoute: Routes = [
  {
    path: ':slug/delete',
    component: TagsDeletePopupAdminComponent,
    resolve: {
      tags: TagsAdminResolve
    },
    data: {
      authorities: ['ROLE_ADMIN'],
      pageTitle: 'jhipsterApp.tags.home.title'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
