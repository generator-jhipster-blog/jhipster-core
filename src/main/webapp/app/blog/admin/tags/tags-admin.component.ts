import { Component, ViewChild } from '@angular/core';

import { TagsListAdminComponent } from './tags-list-admin.component';

@Component({
  selector: 'jhi-blog-admin-tags',
  templateUrl: './tags-admin.component.html'
})
export class TagsAdminComponent {
  @ViewChild(TagsListAdminComponent, { static: false }) tagListComponent: TagsListAdminComponent;

  catchNewTag() {
    this.tagListComponent.loadAll();
  }

  previousState() {
    window.history.back();
  }
}
