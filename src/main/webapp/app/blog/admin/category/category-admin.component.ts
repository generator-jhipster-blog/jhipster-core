import { Component, ViewChild } from '@angular/core';

import { CategoryListAdminComponent } from './category-list-admin.component';

@Component({
  selector: 'jhi-blog-admin-category',
  templateUrl: './category-admin.component.html'
})
export class CategoryAdminComponent {
  @ViewChild(CategoryListAdminComponent, { static: false }) categoryListComponent: CategoryListAdminComponent;

  catchNewCategory() {
    this.categoryListComponent.loadAll();
  }

  previousState() {
    window.history.back();
  }
}
