import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { AngularEditorFaModule } from '@gsig/angular-editor-fa';

import { JhipsterSharedModule } from 'app/shared/shared.module';

import { CategoryAdminComponent } from './category-admin.component';
import { CategoryUpdateAdminComponent } from './category-update-admin.component';
import { CategoryDeleteDialogAdminComponent, CategoryDeletePopupAdminComponent } from './category-delete-dialog-admin.component';
import { categoryPopupAdminRoute, categoryAdminRoute } from './category-admin.route';
import { CategoryListAdminComponent } from './category-list-admin.component';
import { JhipsterBlogSharedModule } from '../../shared/blog.shared.module';

const ENTITY_STATES = [...categoryAdminRoute, ...categoryPopupAdminRoute];

@NgModule({
  imports: [JhipsterSharedModule, RouterModule.forChild(ENTITY_STATES), JhipsterBlogSharedModule, AngularEditorFaModule],
  declarations: [
    CategoryAdminComponent,
    CategoryListAdminComponent,
    CategoryUpdateAdminComponent,
    CategoryDeleteDialogAdminComponent,
    CategoryDeletePopupAdminComponent
  ],
  entryComponents: [CategoryDeleteDialogAdminComponent]
})
export class JhipsterCategoryAdminModule {}
