import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiResolvePagingParams } from 'ng-jhipster';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';

import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';

import { Category, ICategory } from '../../shared/model/category.model';
import { CategoryService } from '../../service/category.service';
import { CategoryAdminComponent } from './category-admin.component';
import { CategoryDeletePopupAdminComponent } from './category-delete-dialog-admin.component';

@Injectable({ providedIn: 'root' })
export class CategoryAdminResolve implements Resolve<ICategory> {
  constructor(private service: CategoryService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<ICategory> {
    const slug = route.params['slug'];
    if (slug) {
      return this.service.find(slug).pipe(
        filter((response: HttpResponse<Category>) => response.ok),
        map((category: HttpResponse<Category>) => category.body)
      );
    }
    return of(new Category());
  }
}

export const categoryAdminRoute: Routes = [
  {
    path: '',
    component: CategoryAdminComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams,
      category: CategoryAdminResolve
    },
    data: {
      authorities: ['ROLE_ADMIN'],
      defaultSort: 'id,asc',
      pageTitle: 'jhipsterApp.category.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':slug/edit',
    component: CategoryAdminComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams,
      category: CategoryAdminResolve
    },
    data: {
      authorities: ['ROLE_ADMIN'],
      pageTitle: 'jhipsterApp.category.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const categoryPopupAdminRoute: Routes = [
  {
    path: ':slug/delete',
    component: CategoryDeletePopupAdminComponent,
    resolve: {
      category: CategoryAdminResolve
    },
    data: {
      authorities: ['ROLE_ADMIN'],
      pageTitle: 'jhipsterApp.category.home.title'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
