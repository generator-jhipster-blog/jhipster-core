import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { JhiAlertService } from 'ng-jhipster';
import { IAngularEditorConfig } from '@gsig/angular-editor-fa';

import { Category, ICategory } from '../../shared/model/category.model';
import { REGEX_SLUG } from '../../blog.constants';
import { SlugUtil } from '../../shared/util/slug-util';
import { CategoryService } from '../../service/category.service';
import { WysiwygUtil } from 'app/blog/shared/util/wysiwyg-util';

@Component({
  selector: 'jhi-blog-admin-category-update',
  templateUrl: './category-update-admin.component.html'
})
export class CategoryUpdateAdminComponent implements OnInit {
  isSaving: boolean;

  categories: ICategory[];

  editorConfig: IAngularEditorConfig = WysiwygUtil.configSimple();

  editForm = this.fb.group({
    id: [],
    name: [null, [Validators.required, Validators.minLength(3)]],
    slug: [null, [Validators.required, Validators.minLength(3), Validators.pattern(REGEX_SLUG)]],
    description: [],
    parentCategoryId: []
  });

  @Output() createdCategory = new EventEmitter<any>();

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected categoryService: CategoryService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  getService(): CategoryService {
    return this.categoryService;
  }

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ category }) => {
      this.updateForm(category);
    });
  }

  updateForm(category: ICategory) {
    this.editForm.patchValue({
      id: category.id,
      name: category.name,
      slug: category.slug,
      description: category.description,
      parentCategoryId: category.parentCategoryId
    });
  }

  save() {
    this.isSaving = true;
    const category = this.createFromForm();
    if (category.id !== undefined && category.id !== null) {
      this.subscribeToSaveResponse(this.categoryService.update(category), category.id);
    } else {
      this.subscribeToSaveResponse(this.categoryService.create(category), category.id);
    }
  }

  private createFromForm(): ICategory {
    return {
      ...new Category(),
      id: this.editForm.get(['id']).value,
      name: this.editForm.get(['name']).value,
      slug: this.editForm.get(['slug']).value,
      description: this.editForm.get(['description']).value,
      parentCategoryId: this.editForm.get(['parentCategoryId']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ICategory>>, categoryId: number) {
    result.subscribe(() => this.onSaveSuccess(categoryId), () => this.onSaveError());
  }

  protected onSaveSuccess(categoryId: number) {
    this.isSaving = false;
    this.createdCategory.emit();
    this.resetFrom();
  }

  protected onSaveError() {
    this.isSaving = false;
  }

  onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackCategoryById(index: number, item: ICategory) {
    return item.id;
  }

  buildSlug() {
    this.editForm.controls['slug'].setValue(SlugUtil.slugify(this.editForm.controls['name'].value));
  }

  resetFrom() {
    this.editForm.reset();
  }

  loadCategory(dataCategories: ICategory[]) {
    this.categories = dataCategories;
  }
}
