import { Component, OnInit } from '@angular/core';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { JhiAlertService } from 'ng-jhipster';
import { Comment, IComment } from '../../shared/model/comment.model';
import { CommentService } from '../../service/comment.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CommentDeleteSimpleDialogComponent } from 'app/blog/admin/comment/comment-delete-simple-dialog.component';

@Component({
  selector: 'jhi-blog-admin-comment-update',
  templateUrl: './comment-update-admin.component.html'
})
export class CommentUpdateAdminComponent implements OnInit {
  isSaving: boolean;

  editForm = this.fb.group({
    id: [],
    username: [null, [Validators.required]],
    content: [null, [Validators.required]],
    postId: [null, [Validators.required]]
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected commentService: CommentService,
    protected activatedRoute: ActivatedRoute,
    protected modalService: NgbModal,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ comment }) => {
      this.updateForm(comment);
    });
  }

  updateForm(comment: IComment) {
    this.editForm.patchValue({
      id: comment.id,
      username: comment.username,
      content: comment.content,
      postId: comment.postId
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const comment = this.createFromForm();
    if (comment.id !== undefined) {
      this.subscribeToSaveResponse(this.commentService.update(comment));
    } else {
      this.subscribeToSaveResponse(this.commentService.create(comment));
    }
  }

  private createFromForm(): IComment {
    return {
      ...new Comment(),
      id: this.editForm.get(['id']).value,
      username: this.editForm.get(['username']).value,
      content: this.editForm.get(['content']).value,
      postId: this.editForm.get(['postId']).value
    };
  }

  deleteComment() {
    const modalRef = this.modalService.open(CommentDeleteSimpleDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.commentId = this.editForm.get(['id']).value;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IComment>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }

  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }
}
