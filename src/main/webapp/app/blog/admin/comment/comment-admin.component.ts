import { Component } from '@angular/core';

@Component({
  selector: 'jhi-blog-admin-comment',
  templateUrl: './comment-admin.component.html'
})
export class CommentAdminComponent {
  previousState() {
    window.history.back();
  }
}
