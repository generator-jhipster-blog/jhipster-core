import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';

import { Comment, IComment } from '../../shared/model/comment.model';
import { CommentService } from '../../service/comment.service';
import { CommentDeletePopupAdminComponent } from './comment-delete-dialog-admin.component';
import { CommentAdminComponent } from './comment-admin.component';
import { CommentUpdateAdminComponent } from './comment-update-admin.component';

@Injectable({ providedIn: 'root' })
export class CommentAdminResolve implements Resolve<IComment> {
  constructor(private service: CommentService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IComment> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<Comment>) => response.ok),
        map((comment: HttpResponse<Comment>) => comment.body)
      );
    }
    return of(new Comment());
  }
}

export const commentAdminRoute: Routes = [
  {
    path: '',
    component: CommentAdminComponent,
    data: {
      authorities: ['ROLE_ADMIN'],
      pageTitle: 'jhipsterApp.comment.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: CommentUpdateAdminComponent,
    resolve: {
      comment: CommentAdminResolve
    },
    data: {
      authorities: ['ROLE_ADMIN'],
      pageTitle: 'jhipsterApp.comment.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const commentAdminPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: CommentDeletePopupAdminComponent,
    resolve: {
      comment: CommentAdminResolve
    },
    data: {
      authorities: ['ROLE_ADMIN'],
      pageTitle: 'jhipsterApp.comment.home.title'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
