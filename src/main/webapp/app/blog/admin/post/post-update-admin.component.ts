import { Component, ElementRef, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { JhiAlertService, JhiDataUtils } from 'ng-jhipster';
import { IAngularEditorConfig } from '@gsig/angular-editor-fa';

import { IPost, Post } from '../../shared/model/post.model';
import { PostService } from '../../service/post.service';
import { ITags } from '../../shared/model/tags.model';
import { TagsService } from '../../service/tags.service';
import { ICategory } from '../../shared/model/category.model';
import { CategoryService } from '../../service/category.service';
import { SlugUtil } from '../../shared/util/slug-util';
import { WysiwygUtil } from '../../shared/util/wysiwyg-util';

@Component({
  selector: 'jhi-blog-admin-post-update',
  templateUrl: './post-update-admin.component.html'
})
export class PostUpdateAdminComponent implements OnInit {
  isSaving: boolean;

  tags: ITags[];

  selectedTags: ITags[];

  categories: ICategory[];

  editorConfig: IAngularEditorConfig = WysiwygUtil.configSimple();

  editForm = this.fb.group({
    id: [],
    slug: [null, [Validators.required, Validators.minLength(3), Validators.pattern('^[a-z0-9]+(?:-[a-z0-9]+)*$')]],
    title: [null, [Validators.required, Validators.minLength(3)]],
    description: [Validators.minLength(3)],
    publish: [],
    featuredImage: [],
    featuredImageContentType: [],
    content: [],
    tags: [],
    categoryId: []
  });

  constructor(
    protected dataUtils: JhiDataUtils,
    protected jhiAlertService: JhiAlertService,
    protected postService: PostService,
    protected tagsService: TagsService,
    protected categoryService: CategoryService,
    protected elementRef: ElementRef,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {
    this.selectedTags = [];
  }

  getCategoryService(): CategoryService {
    return this.categoryService;
  }

  getTagsService(): TagsService {
    return this.tagsService;
  }

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ post }) => {
      this.updateForm(post);
    });
  }

  loadTags(dataTags: ITags[]) {
    dataTags.forEach(dt => {
      if (undefined !== this.selectedTags && undefined !== this.selectedTags.find(st => st.id === dt.id)) {
        dt.selectedItem = true;
      }
    });
    this.tags = dataTags;
  }

  loadCategory(dataCategories: ICategory[]) {
    this.categories = dataCategories;
  }

  updateForm(post: IPost) {
    if (undefined !== post.tags && post.tags.length > 0) {
      this.selectedTags = post.tags;
    }
    this.editForm.patchValue({
      id: post.id,
      slug: post.slug,
      title: post.title,
      description: post.description,
      publish: post.publish,
      featuredImage: post.featuredImage,
      featuredImageContentType: post.featuredImageContentType,
      content: post.content,
      tags: post.tags,
      categoryId: post.categoryId
    });
  }

  byteSize(field) {
    return this.dataUtils.byteSize(field);
  }

  openFile(contentType, field) {
    return this.dataUtils.openFile(contentType, field);
  }

  setFileData(event, field: string, isImage) {
    return new Promise((resolve, reject) => {
      if (event && event.target && event.target.files && event.target.files[0]) {
        const file: File = event.target.files[0];
        if (isImage && !file.type.startsWith('image/')) {
          reject(`File was expected to be an image but was found to be ${file.type}`);
        } else {
          const filedContentType: string = field + 'ContentType';
          this.dataUtils.toBase64(file, base64Data => {
            this.editForm.patchValue({
              [field]: base64Data,
              [filedContentType]: file.type
            });
          });
        }
      } else {
        reject(`Base64 data was not set as file could not be extracted from passed parameter: ${event}`);
      }
    }).then(
      // eslint-disable-next-line no-console
      () => console.log('blob added'), // success
      this.onError
    );
  }

  clearInputImage(field: string, fieldContentType: string, idInput: string) {
    this.editForm.patchValue({
      [field]: null,
      [fieldContentType]: null
    });
    if (this.elementRef && idInput && this.elementRef.nativeElement.querySelector('#' + idInput)) {
      this.elementRef.nativeElement.querySelector('#' + idInput).value = null;
    }
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const post = this.createFromForm();
    if (post.id !== undefined) {
      this.subscribeToSaveResponse(this.postService.update(post));
    } else {
      this.subscribeToSaveResponse(this.postService.create(post));
    }
  }

  private createFromForm(): IPost {
    return {
      ...new Post(),
      id: this.editForm.get(['id']).value,
      slug: this.editForm.get(['slug']).value,
      title: this.editForm.get(['title']).value,
      description: this.editForm.get(['description']).value,
      publish: this.editForm.get(['publish']).value,
      featuredImageContentType: this.editForm.get(['featuredImageContentType']).value,
      featuredImage: this.editForm.get(['featuredImage']).value,
      content: this.editForm.get(['content']).value,
      tags: this.editForm.get(['tags']).value,
      categoryId: this.editForm.get(['categoryId']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IPost>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }

  onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackTagsById(index: number, item: ITags) {
    return item.id;
  }

  trackCategoryById(index: number, item: ICategory) {
    return item.id;
  }

  buildSlug() {
    this.editForm.controls['slug'].setValue(SlugUtil.slugify(this.editForm.controls['title'].value));
  }

  onSelectTag(tagsOption: ITags) {
    if (undefined === this.selectedTags.find(st => st.id === tagsOption.id)) {
      tagsOption.selectedItem = true;
      this.selectedTags.push(tagsOption);
    } else {
      tagsOption.selectedItem = false;
      this.selectedTags = this.selectedTags.filter(st => st.id !== tagsOption.id);
    }
    this.editForm.controls['tags'].setValue(this.selectedTags);
  }
}
