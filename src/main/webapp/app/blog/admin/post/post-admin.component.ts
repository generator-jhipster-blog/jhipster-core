import { Component } from '@angular/core';

@Component({
  selector: 'jhi-blog-admin-post',
  templateUrl: './post-admin.component.html'
})
export class PostAdminComponent {
  previousState() {
    window.history.back();
  }
}
