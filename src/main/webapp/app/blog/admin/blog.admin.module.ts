import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'dashboard',
        loadChildren: () => import('./dashboard/dashboard.admin.module').then(m => m.JhipsterDashboardAdminModule)
      },
      {
        path: 'tags',
        loadChildren: () => import('./tags/tags.admin.module').then(m => m.JhipsterTagsAdminModule)
      },
      {
        path: 'category',
        loadChildren: () => import('./category/category.admin.module').then(m => m.JhipsterCategoryAdminModule)
      },
      {
        path: 'post',
        loadChildren: () => import('./post/post.admin.module').then(m => m.JhipsterPostAdminModule)
      },
      {
        path: 'comment',
        loadChildren: () => import('./comment/comment.admin.module').then(m => m.JhipsterCommentAdminModule)
      }
    ])
  ]
})
export class JhipsterBlogAdminModule {}
