import { ITags } from './tags.model';

export interface IPost {
  id?: number;
  slug?: string;
  title?: string;
  publish?: boolean;
  featuredImageContentType?: string;
  featuredImage?: any;
  description?: string;
  content?: any;
  tags?: ITags[];
  categoryName?: string;
  categoryId?: number;
  categorySlug?: string;
  createdBy?: string;
  createdDate?: Date;
  lastModifiedBy?: string;
  lastModifiedDate?: Date;
}

export class Post implements IPost {
  constructor(
    public id?: number,
    public slug?: string,
    public title?: string,
    public publish?: boolean,
    public featuredImageContentType?: string,
    public featuredImage?: any,
    public description?: string,
    public content?: any,
    public tags?: ITags[],
    public categoryName?: string,
    public categoryId?: number,
    public createdBy?: string,
    public createdDate?: Date,
    public lastModifiedBy?: string,
    public lastModifiedDate?: Date
  ) {
    this.publish = this.publish || true;
    this.createdBy = createdBy ? createdBy : null;
    this.createdDate = createdDate ? createdDate : null;
    this.lastModifiedBy = lastModifiedBy ? lastModifiedBy : null;
    this.lastModifiedDate = lastModifiedDate ? lastModifiedDate : null;
  }
}
