import { ICategory } from './category.model';

export interface ICategory {
  id?: number;
  name?: string;
  slug?: string;
  description?: any;
  parentCategoryName?: string;
  parentCategorySlug?: string;
  parentCategoryId?: number;
  childrens?: ICategory[];
}

export class Category implements ICategory {
  constructor(
    public id?: number,
    public name?: string,
    public slug?: string,
    public description?: any,
    public parentCategoryName?: string,
    public parentCategorySlug?: string,
    public parentCategoryId?: number,
    public childrens?: ICategory[]
  ) {
    this.childrens = [];
  }
}
