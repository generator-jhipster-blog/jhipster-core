import { NgModule } from '@angular/core';
import { JhipsterSharedModule } from 'app/shared/shared.module';
import { RouterModule } from '@angular/router';

import { SelectPaginateComponent } from './component/select-paginate.component';
import { SubnavbarAdminComponent } from './component/subnavbar-admin.component';
import { SelectHierarchicalComponent } from './component/select-hierarchical.component';
import { CategoryFilterWidgetComponent } from './widget/filter/category-filter-widget.component';
import { TagsFilterWidgetComponent } from './widget/filter/tags-filter-widget.component';
import { PostListTabViewWidgetComponent } from './widget/view/post-list-tab-view-widget.component';
import { PostListCardViewWidgetComponent } from './widget/view/post-list-card-view-widget.component';
import { CommonSearchWidgetComponent } from './widget/search/common-search-widget.component';
import { PostSearchWidgetComponent } from './widget/search/post-search-widget.component';
import { CategoryMenuWidgetComponent } from './widget/menu/category-menu-widget.component';
import { TagsMenuWidgetComponent } from './widget/menu/tags-menu-widget.component';

@NgModule({
  imports: [JhipsterSharedModule, RouterModule.forChild([])],
  declarations: [
    PostListTabViewWidgetComponent,
    SelectHierarchicalComponent,
    SelectPaginateComponent,
    PostSearchWidgetComponent,
    CommonSearchWidgetComponent,
    PostListCardViewWidgetComponent,
    TagsFilterWidgetComponent,
    CategoryFilterWidgetComponent,
    CategoryMenuWidgetComponent,
    TagsMenuWidgetComponent,
    SubnavbarAdminComponent
  ],
  exports: [
    PostListTabViewWidgetComponent,
    SelectHierarchicalComponent,
    SelectPaginateComponent,
    PostSearchWidgetComponent,
    CommonSearchWidgetComponent,
    PostListCardViewWidgetComponent,
    CategoryFilterWidgetComponent,
    TagsFilterWidgetComponent,
    CategoryMenuWidgetComponent,
    TagsMenuWidgetComponent,
    SubnavbarAdminComponent
  ]
})
export class JhipsterBlogSharedModule {}
