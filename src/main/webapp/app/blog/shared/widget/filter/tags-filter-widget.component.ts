import { Component, EventEmitter, Input, Output } from '@angular/core';

import { TagsService } from '../../../service/tags.service';
import { ITags } from '../../../shared/model/tags.model';

/**
 * Widget for filtering current by tags.
 */
@Component({
  selector: 'jhi-blog-widget-tags-filter',
  templateUrl: './tags-filter-widget.component.html'
})
export class TagsFilterWidgetComponent {
  @Input() queryRequest: any;

  @Output() onTagsChange = new EventEmitter<any>();

  tags: ITags[];

  constructor(protected tagsService: TagsService) {}

  getService(): TagsService {
    return this.tagsService;
  }

  trackId(index: number, item: ITags) {
    return item.id;
  }

  onSelectTags(tag: ITags) {
    this.queryRequest['tagsId.equals'] = tag.id;
    this.queryRequest['tagsName.equals'] = encodeURIComponent(tag.name);
    this.onTagsChange.emit(this.queryRequest);
  }

  onSelectAllTags() {
    delete this.queryRequest['tagsId.equals'];
    delete this.queryRequest['tagsName.equals'];
    this.onTagsChange.emit(this.queryRequest);
  }

  isActiveItem(tagsId: number): boolean {
    return this.queryRequest['tagsId.equals'] && parseInt(this.queryRequest['tagsId.equals'], 10) === tagsId;
  }

  loadTags(dataTags: ITags[]) {
    this.tags = dataTags;
  }

  onError(errorMessage: string) {
    console.error(errorMessage);
  }
}
