import { Component } from '@angular/core';
import { JhiParseLinks } from 'ng-jhipster';
import { AccountService } from 'app/core/auth/account.service';
import { PostService } from '../../../service/post.service';
import { PostListViewAbstract } from './post-list-view-abstract.component';

@Component({
  selector: 'jhi-blog-widget-post-list-tab-view',
  templateUrl: './post-list-tab-view-widget.component.html'
})
export class PostListTabViewWidgetComponent extends PostListViewAbstract {
  constructor(protected postService: PostService, protected parseLinks: JhiParseLinks, protected accountService: AccountService) {
    super(postService, parseLinks, accountService);
  }
}
