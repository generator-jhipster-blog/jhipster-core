import { Component } from '@angular/core';
import { JhiParseLinks } from 'ng-jhipster';
import { AccountService } from 'app/core/auth/account.service';
import { PostService } from '../../../service/post.service';
import { PostListViewAbstract } from './post-list-view-abstract.component';

@Component({
  selector: 'jhi-blog-widget-post-list-card-view',
  templateUrl: './post-list-card-view-widget.component.html'
})
export class PostListCardViewWidgetComponent extends PostListViewAbstract {
  constructor(protected postService: PostService, protected parseLinks: JhiParseLinks, protected accountService: AccountService) {
    super(postService, parseLinks, accountService);
  }
}
