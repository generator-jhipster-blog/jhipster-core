import { EventEmitter, Input, OnDestroy, Output } from '@angular/core';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { JhiParseLinks } from 'ng-jhipster';

import { ITEMS_PER_PAGE } from 'app/shared/constants/pagination.constants';
import { AccountService } from 'app/core/auth/account.service';

import { IPost } from '../../model/post.model';
import { PostService } from '../../../service/post.service';

/**
 * Abstract class for post list.
 */
export class PostListViewAbstract implements OnDestroy {
  isLoading: boolean;
  posts: IPost[];
  error: any;
  success: any;
  links: any;
  totalItems: any;
  itemsPerPage: any;
  page: any;
  predicate: any;
  previousPage: any;
  reverse: any;
  private _query: any;

  get query(): any {
    return this._query;
  }

  /**
   * REST request parameters.
   * Search/Filtering instructions.
   *
   * Exemple ['tagsId.equals:1']
   */
  @Input() set query(value: any) {
    this._query = value;
    if (!value.size) {
      this._query.size = ITEMS_PER_PAGE;
    }
    if (value.page) {
      this.page = value.page;
    }
    this.loadAll();
  }

  @Output() queryChange = new EventEmitter<any>();

  /**
   * Return route request with updated parameters.
   */
  @Output() routePageChange = new EventEmitter<any>();

  constructor(protected postService: PostService, protected parseLinks: JhiParseLinks, protected accountService: AccountService) {
    this.itemsPerPage = ITEMS_PER_PAGE;
    this.page = 1;
    this.isLoading = false;
  }

  loadAll() {
    this.isLoading = true;

    const serverQuery = this.query;
    serverQuery.page = this.page - 1;
    serverQuery.sort = ['slug,asc'];

    this.postService
      .search(serverQuery)
      .subscribe((res: HttpResponse<IPost[]>) => this.paginatePosts(res.body, res.headers), () => (this.isLoading = false));
  }

  loadPage(page: number) {
    if (page !== this.query.previousPage) {
      this.query.previousPage = page;
      this.loadAll();
      this.routePageChange.emit(this.query);
    }
  }

  trackId(index: number, item: IPost) {
    return item.id;
  }

  paginatePosts(data: IPost[], headers: HttpHeaders) {
    this.links = this.parseLinks.parse(headers.get('link'));
    this.totalItems = parseInt(headers.get('X-Total-Count'), 10);
    this.posts = data;
    this.isLoading = false;
  }

  ngOnDestroy(): void {
    this.queryChange.unsubscribe();
    this.routePageChange.unsubscribe();
  }
}
