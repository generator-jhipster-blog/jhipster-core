import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { JhipsterBlogSharedModule } from './shared/blog.shared.module';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'blog',
        loadChildren: () => import('./public/blog.public.module').then(m => m.JhipsterBlogPublicModule)
      },
      {
        path: 'blog/admin',
        loadChildren: () => import('./admin/blog.admin.module').then(m => m.JhipsterBlogAdminModule)
      }
    ]),
    JhipsterBlogSharedModule
  ]
})
export class JhipsterBlogModule {}
