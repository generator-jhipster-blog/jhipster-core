import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { JhipsterSharedModule } from 'app/shared/shared.module';

import { CategoryDetailPublicComponent } from './category-detail-public.component';
import { categoryPublicRoute } from './category-public.route';
import { JhipsterBlogSharedModule } from '../../shared/blog.shared.module';
import { CategoryListPublicComponent } from './category-list-public.component';

const ENTITY_STATES = [...categoryPublicRoute];

@NgModule({
  imports: [JhipsterSharedModule, JhipsterBlogSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [CategoryDetailPublicComponent, CategoryListPublicComponent]
})
export class JhipsterCategoryPublicModule {}
