import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot, Routes } from '@angular/router';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiResolvePagingParams } from 'ng-jhipster';

import { Category, ICategory } from '../../shared/model/category.model';
import { CategoryDetailPublicComponent } from './category-detail-public.component';
import { CategoryService } from '../../service/category.service';
import { CategoryListPublicComponent } from './category-list-public.component';

@Injectable({ providedIn: 'root' })
export class CategoryPublicResolve implements Resolve<ICategory> {
  constructor(private service: CategoryService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<ICategory> {
    const slug = route.params['slug'];
    if (slug) {
      return this.service.find(slug).pipe(
        filter((response: HttpResponse<Category>) => response.ok),
        map((category: HttpResponse<Category>) => category.body)
      );
    }
    return of(new Category());
  }
}

export const categoryPublicRoute: Routes = [
  {
    path: '',
    component: CategoryListPublicComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      defaultSort: 'name,asc',
      pageTitle: 'jhipsterApp.category.home.title'
    }
  },
  {
    path: ':slug',
    component: CategoryDetailPublicComponent,
    resolve: {
      category: CategoryPublicResolve
    },
    data: {
      pageTitle: 'jhipsterApp.category.home.title'
    }
  }
];
