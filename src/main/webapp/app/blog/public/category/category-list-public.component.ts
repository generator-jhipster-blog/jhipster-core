import { Component, OnInit } from '@angular/core';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { JhiParseLinks } from 'ng-jhipster';

import { ITEMS_PER_PAGE } from 'app/shared/constants/pagination.constants';

import { ICategory } from '../../shared/model/category.model';
import { CategoryService } from '../../service/category.service';
import { Search } from '../../shared/model/search.model';

@Component({
  selector: 'jhi-blog-public-category-list',
  templateUrl: './category-list-public.component.html'
})
export class CategoryListPublicComponent implements OnInit {
  category: ICategory[];
  error: any;
  success: any;
  routeData: any;
  links: any;
  totalItems: any;
  itemsPerPage: any;
  page: any;
  predicate: any;
  previousPage: any;
  reverse: any;
  isLoading: boolean;
  searchPhrase: string;

  constructor(
    protected categoryService: CategoryService,
    protected parseLinks: JhiParseLinks,
    protected activatedRoute: ActivatedRoute,
    protected router: Router
  ) {
    this.isLoading = false;
    this.searchPhrase = null;
    this.itemsPerPage = ITEMS_PER_PAGE;
    this.routeData = this.activatedRoute.data.subscribe(data => {
      this.page = data.pagingParams.page;
      this.previousPage = data.pagingParams.page;
      this.reverse = data.pagingParams.ascending;
      this.predicate = data.pagingParams.predicate;
    });
  }

  loadAll() {
    this.isLoading = true;

    const requestParam = {
      page: this.page - 1,
      size: this.itemsPerPage,
      sort: this.sort()
    };

    if (null === this.searchPhrase) {
      this.categoryService
        .query(requestParam)
        .subscribe((res: HttpResponse<ICategory[]>) => this.paginateTags(res.body, res.headers), () => (this.isLoading = false));
    } else {
      this.categoryService
        .searchFull(new Search(this.searchPhrase), requestParam)
        .subscribe((res: HttpResponse<ICategory[]>) => this.paginateTags(res.body, res.headers), () => (this.isLoading = false));
    }
  }

  loadPage(page: number) {
    if (page !== this.previousPage) {
      this.previousPage = page;
      this.transition();
    }
  }

  transition() {
    this.router.navigate(['/blog/category'], {
      queryParams: {
        page: this.page,
        size: this.itemsPerPage,
        sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
      }
    });
    this.loadAll();
  }

  clear() {
    this.page = 0;
    this.router.navigate([
      '/blog/category',
      {
        page: this.page,
        sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
      }
    ]);
    this.loadAll();
  }

  ngOnInit() {
    this.loadAll();
  }

  trackId(index: number, item: ICategory) {
    return item.id;
  }

  sort() {
    const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
    if (this.predicate !== 'id') {
      result.push('id');
    }
    return result;
  }

  protected paginateTags(data: ICategory[], headers: HttpHeaders) {
    this.links = this.parseLinks.parse(headers.get('link'));
    this.totalItems = parseInt(headers.get('X-Total-Count'), 10);
    this.category = data;
    this.isLoading = false;
  }

  onSearchTerm(searchTerm: string) {
    this.searchPhrase = searchTerm;
    this.clear();
  }

  onSearchReset() {
    this.searchPhrase = null;
    this.clear();
  }
}
