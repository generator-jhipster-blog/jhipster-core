import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IPost } from '../../shared/model/post.model';
import { SeoService } from '../../service/seo.service';

@Component({
  selector: 'jhi-blog-public-post-detail',
  templateUrl: './post-detail-public.component.html'
})
export class PostDetailPublicComponent implements OnInit {
  post: IPost;

  constructor(protected activatedRoute: ActivatedRoute, protected seoService: SeoService) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ post }) => {
      this.post = post;
      this.seoService.addMetaForPost(this.post);
    });
  }

  previousState() {
    window.history.back();
  }
}
