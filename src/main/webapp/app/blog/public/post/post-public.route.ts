import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot, Routes } from '@angular/router';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiResolvePagingParams } from 'ng-jhipster';

import { IPost, Post } from '../../shared/model/post.model';
import { PostService } from '../../service/post.service';
import { PostDetailPublicComponent } from './post-detail-public.component';
import { PostListPublicComponent } from './post-list-public.component';

@Injectable({ providedIn: 'root' })
export class PostPublicResolve implements Resolve<IPost> {
  constructor(private service: PostService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IPost> {
    const slug = route.params['slug'];
    if (slug) {
      return this.service.find(slug).pipe(
        filter((response: HttpResponse<Post>) => response.ok),
        map((post: HttpResponse<Post>) => post.body)
      );
    }
    return of(new Post());
  }
}

export const postPublicRoute: Routes = [
  {
    path: '',
    component: PostListPublicComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: [],
      defaultSort: 'slug,asc',
      pageTitle: 'jhipsterApp.post.home.title'
    }
  },
  {
    path: ':slug',
    component: PostDetailPublicComponent,
    resolve: {
      post: PostPublicResolve
    },
    data: {
      authorities: [],
      pageTitle: 'jhipsterApp.post.home.title'
    }
  }
];
