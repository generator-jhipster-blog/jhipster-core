import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { ITags } from '../../shared/model/tags.model';

@Component({
  selector: 'jhi-blog-public-tags-detail',
  templateUrl: './tags-detail-public.component.html'
})
export class TagsDetailPublicComponent implements OnInit {
  tags: ITags;
  requestParam: any;
  routeData: any;

  constructor(protected router: Router, protected activatedRoute: ActivatedRoute) {
    this.requestParam = {};
  }

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ tags }) => {
      this.tags = tags;
      // fix : force angular detection when object change
      this.requestParam = Object.assign({}, this.requestParam);
      this.requestParam['tagsId.equals'] = tags.id;
    });
  }

  transition(routeRequest: any) {
    this.requestParam = routeRequest;
    this.router.navigate(['/blog/tags', this.tags.slug], { queryParams: routeRequest });
  }

  previousState() {
    window.history.back();
  }
}
