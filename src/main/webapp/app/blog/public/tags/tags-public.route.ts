import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot, Routes } from '@angular/router';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiResolvePagingParams } from 'ng-jhipster';

import { ITags, Tags } from '../../shared/model/tags.model';
import { TagsDetailPublicComponent } from './tags-detail-public.component';
import { TagsService } from '../../service/tags.service';
import { TagsListPublicComponent } from './tags-list-public.component';

@Injectable({ providedIn: 'root' })
export class TagsPublicResolve implements Resolve<ITags> {
  constructor(private service: TagsService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<ITags> {
    const slug = route.params['slug'];
    if (slug) {
      return this.service.find(slug).pipe(
        filter((response: HttpResponse<Tags>) => response.ok),
        map((tags: HttpResponse<Tags>) => tags.body)
      );
    }
    return of(new Tags());
  }
}

export const tagsPublicRoute: Routes = [
  {
    path: '',
    component: TagsListPublicComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      defaultSort: 'name,asc',
      pageTitle: 'jhipsterApp.tags.home.title'
    }
  },
  {
    path: ':slug',
    component: TagsDetailPublicComponent,
    resolve: {
      tags: TagsPublicResolve
    },
    data: {
      pageTitle: 'jhipsterApp.tags.home.title'
    }
  }
];
