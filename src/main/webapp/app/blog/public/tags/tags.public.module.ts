import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { JhipsterSharedModule } from 'app/shared/shared.module';

import { TagsDetailPublicComponent } from './tags-detail-public.component';
import { tagsPublicRoute } from './tags-public.route';
import { JhipsterBlogSharedModule } from '../../shared/blog.shared.module';
import { TagsListPublicComponent } from './tags-list-public.component';

const ENTITY_STATES = [...tagsPublicRoute];

@NgModule({
  imports: [JhipsterSharedModule, JhipsterBlogSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [TagsDetailPublicComponent, TagsListPublicComponent]
})
export class JhipsterTagsPublicModule {}
