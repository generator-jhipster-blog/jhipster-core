import { Component, Input, OnInit } from '@angular/core';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { JhiAlertService, JhiEventManager } from 'ng-jhipster';
import { Comment, IComment } from '../../shared/model/comment.model';
import { CommentService } from '../../service/comment.service';
import { IPost } from '../../shared/model/post.model';

@Component({
  selector: 'jhi-blog-public-comment-create',
  templateUrl: './comment-create-public.component.html'
})
export class CommentCreatePublicComponent implements OnInit {
  isSaving: boolean;

  posts: IPost[];

  editForm = this.fb.group({
    content: [null, [Validators.required]]
  });

  @Input() postId: number;

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected commentService: CommentService,
    protected activatedRoute: ActivatedRoute,
    protected eventManager: JhiEventManager,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
  }

  save() {
    this.isSaving = true;
    const comment = this.createFromForm();
    this.subscribeToSaveResponse(this.commentService.create(comment));
  }

  private createFromForm(): IComment {
    return {
      ...new Comment(),
      content: this.editForm.get(['content']).value,
      postId: this.postId
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IComment>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.editForm = this.fb.group({
      content: [null, [Validators.required]]
    });
    this.eventManager.broadcast({ name: 'commentListModification', content: 'Create a comment' });
  }

  protected onSaveError() {
    this.isSaving = false;
  }

  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }
}
