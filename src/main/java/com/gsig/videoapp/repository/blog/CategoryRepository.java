package com.gsig.videoapp.repository.blog;

import com.gsig.videoapp.domain.blog.Category;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;


/**
 * Spring Data  repository for the Category entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CategoryRepository extends JpaRepository<Category, Long> {

    /**
     * Find by name or slug.
     *
     * @param name category name
     * @param slug category slug
     * @return optional category
     */
    Optional<Category> findOneByNameOrSlug(String name, String slug);

    /**
     * Find only parents.
     *
     * @param pageable spring page
     * @return spring page
     */
    Page<Category> findDistinctByParentCategoryIsNull(Pageable pageable);

    Optional<Category> findBySlug(String slug);

    @Query(value = "select distinct category from Category category where category.name like :term or category.description like :term",
        countQuery = "select count(distinct category) from Category category where category.name like :term or category.description like :term")
    Page<Category> searchFull(@Param("term") String term, Pageable pageable);
}
