package com.gsig.videoapp.web.rest.blog.vm;

public class BlogSearchTermVM {

    private String fullTerm;

    public BlogSearchTermVM() {
    }

    public BlogSearchTermVM(String term) {
        this.fullTerm = term;
    }

    public String getFullTerm() {
        return fullTerm;
    }

    public void setFullTerm(String fullTerm) {
        this.fullTerm = fullTerm;
    }

    @Override
    public String toString() {
        return "BlogSearchTerm{" +
            "fullTerm='" + fullTerm + '\'' +
            '}';
    }
}
