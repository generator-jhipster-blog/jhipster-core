/**
 * View Models used by Spring MVC REST controllers.
 */
package com.gsig.videoapp.web.rest.vm;
