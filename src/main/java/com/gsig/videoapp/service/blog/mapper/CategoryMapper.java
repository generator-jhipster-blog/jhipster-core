package com.gsig.videoapp.service.blog.mapper;

import com.gsig.videoapp.domain.blog.Category;
import com.gsig.videoapp.service.blog.dto.CategoryDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Category} and its DTO {@link CategoryDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface CategoryMapper extends BlogEntityMapper<CategoryDTO, Category> {

    @Mapping(source = "parentCategory.id", target = "parentCategoryId")
    @Mapping(source = "parentCategory.name", target = "parentCategoryName")
    @Mapping(source = "parentCategory.slug", target = "parentCategorySlug")
    @Mapping(source = "childCategories", target = "childrens")
    CategoryDTO toDto(Category category);

    @Mapping(source = "parentCategoryId", target = "parentCategory")
    @Mapping(target = "childCategories", ignore = true)
    Category toEntity(CategoryDTO categoryDTO);

    default Category fromId(Long id) {
        if (id == null) {
            return null;
        }
        Category category = new Category();
        category.setId(id);
        return category;
    }
}
