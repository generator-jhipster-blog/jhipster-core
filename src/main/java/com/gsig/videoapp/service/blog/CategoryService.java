package com.gsig.videoapp.service.blog;

import com.gsig.videoapp.domain.blog.Category;
import com.gsig.videoapp.repository.blog.CategoryRepository;
import com.gsig.videoapp.service.blog.dto.CategoryDTO;
import com.gsig.videoapp.service.blog.mapper.CategoryMapper;
import com.gsig.videoapp.web.rest.errors.BadRequestAlertException;
import com.gsig.videoapp.web.rest.blog.errors.UniqueConstraintAlertException;
import com.gsig.videoapp.web.rest.blog.vm.BlogSearchTermVM;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link Category}.
 */
@Service
@Transactional
public class CategoryService {

    private final Logger log = LoggerFactory.getLogger(CategoryService.class);

    private static final String ENTITY_NAME = "category";

    private final CategoryRepository categoryRepository;

    private final CategoryMapper categoryMapper;

    private final PostService postService;

    public CategoryService(CategoryRepository categoryRepository, CategoryMapper categoryMapper, PostService postService) {
        this.categoryRepository = categoryRepository;
        this.categoryMapper = categoryMapper;
        this.postService = postService;
    }

    /**
     * Save a category.
     *
     * @param categoryDTO the entity to save.
     * @return the persisted entity.
     */
    public CategoryDTO save(CategoryDTO categoryDTO) {
        log.debug("Request to save Category : {}", categoryDTO);
        checkConstraintBeforeSave(categoryDTO);
        Category category = categoryMapper.toEntity(categoryDTO);
        category = categoryRepository.save(category);
        return categoryMapper.toDto(category);
    }

    private void checkConstraintBeforeSave(CategoryDTO categoryDTO) {
        if (isExist(categoryDTO)) {
            throw new UniqueConstraintAlertException("A category cannot used existing name or slug", "name/slug", ENTITY_NAME, "attributexists");
        }
        if (null != categoryDTO.getId() && null != categoryDTO.getParentCategoryId() && categoryDTO.getId().equals(categoryDTO.getParentCategoryId())) {
            throw new BadRequestAlertException("This category cannot do a reference to himself", ENTITY_NAME, "infinityrecursive");
        }
    }

    /**
     * Is already exist?
     *
     * @param categoryDTO the entity to test.
     * @return yes or not
     */
    public boolean isExist(CategoryDTO categoryDTO) {
        Optional<Category> categoryFound = categoryRepository.findOneByNameOrSlug(categoryDTO.getName(), categoryDTO.getSlug());
        return categoryFound.filter(category -> !category.getId().equals(categoryDTO.getId())).isPresent();
    }

    /**
     * Get all the categories.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<CategoryDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Categories");
        return categoryRepository.findAll(pageable).map(categoryMapper::toDto);
    }

    /**
     * Get all Hierachical categories.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    public Page<CategoryDTO> findAllHierachical(Pageable pageable) {
        log.debug("Request to get all Hierarchical Categories");
        return categoryRepository.findDistinctByParentCategoryIsNull(pageable).map(categoryMapper::toDto);
    }

    /**
     * Get one category by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<CategoryDTO> findOne(Long id) {
        log.debug("Request to get Category : {}", id);
        return categoryRepository.findById(id).map(categoryMapper::toDto);
    }

    /**
     * Get one category by slug.
     *
     * @param slug the slug of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<CategoryDTO> findOne(String slug) {
        log.debug("Request to get Category : {}", slug);
        return categoryRepository.findBySlug(slug).map(categoryMapper::toDto);
    }

    /**
     * Delete the category by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete Category : {}", id);
        Optional<Category> catFound = categoryRepository.findById(id);
        if (catFound.isPresent()) {
            postService.removeCategory(catFound.get());
            categoryRepository.deleteById(id);
        }
    }

    /**
     * Search on all attributs.
     *
     * @param searchTerm search term
     * @param pageable   spring page
     * @return spring page
     */
    public Page<CategoryDTO> searchFull(BlogSearchTermVM searchTerm, Pageable pageable) {
        log.debug("Request to search all categories with term {}", searchTerm);
        return categoryRepository.searchFull("%".concat(searchTerm.getFullTerm()).concat("%"), pageable).map(categoryMapper::toDto);
    }
}
