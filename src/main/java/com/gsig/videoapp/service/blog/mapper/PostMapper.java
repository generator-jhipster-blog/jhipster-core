package com.gsig.videoapp.service.blog.mapper;

import com.gsig.videoapp.domain.blog.Post;
import com.gsig.videoapp.service.blog.dto.PostDTO;

import com.gsig.videoapp.service.mapper.UserMapper;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link Post} and its DTO {@link PostDTO}.
 */
@Mapper(componentModel = "spring", uses = {TagsMapper.class, CategoryMapper.class, UserMapper.class})
public interface PostMapper extends BlogEntityMapper<PostDTO, Post> {

    @Mapping(source = "category.id", target = "categoryId")
    @Mapping(source = "category.name", target = "categoryName")
    @Mapping(source = "category.slug", target = "categorySlug")
    PostDTO toDto(Post post);

    @Mapping(target = "removeTags", ignore = true)
    @Mapping(source = "categoryId", target = "category")
    @Mapping(target = "createdBy", ignore = true)
    @Mapping(target = "createdDate", ignore = true)
    @Mapping(target = "lastModifiedBy", ignore = true)
    @Mapping(target = "lastModifiedDate", ignore = true)
    Post toEntity(PostDTO postDTO);

    default Post fromId(Long id) {
        if (id == null) {
            return null;
        }
        Post post = new Post();
        post.setId(id);
        return post;
    }
}
