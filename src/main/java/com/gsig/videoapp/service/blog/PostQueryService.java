package com.gsig.videoapp.service.blog;

import com.gsig.videoapp.domain.blog.Category_;
import com.gsig.videoapp.domain.blog.Post;
import com.gsig.videoapp.domain.blog.Post_;
import com.gsig.videoapp.domain.blog.Tags_;
import com.gsig.videoapp.repository.blog.PostRepository;
import com.gsig.videoapp.security.SecurityUtils;
import com.gsig.videoapp.service.blog.dto.PostCriteria;
import com.gsig.videoapp.service.blog.dto.PostDTO;
import com.gsig.videoapp.service.blog.mapper.PostMapper;
import io.github.jhipster.service.QueryService;
import io.github.jhipster.service.filter.BooleanFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.criteria.JoinType;
import java.util.List;

/**
 * Service for executing complex queries for {@link Post} entities in the database.
 * The main input is a {@link PostCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link PostDTO} or a {@link Page} of {@link PostDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class PostQueryService extends QueryService<Post> {

    private final Logger log = LoggerFactory.getLogger(PostQueryService.class);

    private final PostRepository postRepository;

    private final PostMapper postMapper;

    public PostQueryService(PostRepository postRepository, PostMapper postMapper) {
        this.postRepository = postRepository;
        this.postMapper = postMapper;
    }

    /**
     * Return a {@link List} of {@link PostDTO} which matches the criteria from the database.
     *
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<PostDTO> findByCriteria(PostCriteria criteria) {
        log.debug("find by criteria without page : {}", criteria);
        if (criteria.isModeOr()) {
            return postMapper.toDto(postRepository.findAll(createOrSpecification(criteria)));
        }
        if (criteria.isModeFilter()) {
            return postMapper.toDto(postRepository.findAll(createFilterSpecification(criteria)));
        }
        return postMapper.toDto(postRepository.findAll(createAndSpecification(criteria)));
    }

    /**
     * Return a {@link Page} of {@link PostDTO} which matches the criteria from the database.
     *
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page     The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<PostDTO> findByCriteria(PostCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        if (criteria.isModeOr()) {
            return postRepository.findAll(createOrSpecification(criteria), page).map(postMapper::toDto);
        }
        if (criteria.isModeFilter()) {
            return postRepository.findAll(createFilterSpecification(criteria), page).map(postMapper::toDto);
        }
        return postRepository.findAll(createAndSpecification(criteria), page).map(postMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     *
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(PostCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        if (criteria.isModeOr()) {
            return postRepository.count(createOrSpecification(criteria));
        }
        if (criteria.isModeFilter()) {
            return postRepository.count(createFilterSpecification(criteria));
        }
        return postRepository.count(createAndSpecification(criteria));
    }

    /**
     * Function to convert {@link PostCriteria} to a {@link Specification}
     * Specification : Criteria will be build with AND logic operator.
     *
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<Post> createAndSpecification(PostCriteria criteria) {
        Specification<Post> specification = Specification.where(null);

        if (criteria != null) {

            // return only published post for non USER
            if (!SecurityUtils.isAuthenticated()) {
                BooleanFilter isPublish = new BooleanFilter();
                isPublish.setEquals(true);
                criteria.setPublish(isPublish);
            }

            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), Post_.id));
            }
            if (criteria.getSlug() != null) {
                specification = specification.and(buildStringSpecification(criteria.getSlug(), Post_.slug));
            }
            if (criteria.getTitle() != null) {
                specification = specification.and(buildStringSpecification(criteria.getTitle(), Post_.title));
            }
            if (criteria.getDescription() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDescription(), Post_.description));
            }
            if (criteria.getContent() != null) {
                specification = specification.and(buildStringSpecification(criteria.getContent(), Post_.content));
            }
            if (criteria.getTagsId() != null) {
                specification = specification.and(buildSpecification(criteria.getTagsId(),
                    root -> root.join(Post_.tags, JoinType.LEFT).get(Tags_.id)));
            }
            if (criteria.getCategoryId() != null) {
                specification = specification.and(buildSpecification(criteria.getCategoryId(),
                    root -> root.join(Post_.category, JoinType.LEFT).get(Category_.id)));
            }
            if (criteria.getPublish() != null) {
                specification = specification.and(buildSpecification(criteria.getPublish(), Post_.publish));
            }
        }
        return specification;
    }

    /**
     * Function to convert {@link PostCriteria} to a {@link Specification}
     * Specification : Criteria will be build with OR logic operator.
     *
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<Post> createOrSpecification(PostCriteria criteria) {
        Specification<Post> specification = Specification.where(null);

        if (criteria != null) {

            // return only published post for non USER
            if (!SecurityUtils.isAuthenticated()) {
                BooleanFilter isPublish = new BooleanFilter();
                isPublish.setEquals(true);
                criteria.setPublish(isPublish);
            }

            if (criteria.getId() != null) {
                specification = specification.or(buildSpecification(criteria.getId(), Post_.id));
            }
            if (criteria.getSlug() != null) {
                specification = specification.or(buildStringSpecification(criteria.getSlug(), Post_.slug));
            }
            if (criteria.getTitle() != null) {
                specification = specification.or(buildStringSpecification(criteria.getTitle(), Post_.title));
            }
            if (criteria.getDescription() != null) {
                specification = specification.or(buildStringSpecification(criteria.getDescription(), Post_.description));
            }
            if (criteria.getContent() != null) {
                specification = specification.or(buildStringSpecification(criteria.getContent(), Post_.content));
            }
            if (criteria.getTagsId() != null) {
                specification = specification.or(buildSpecification(criteria.getTagsId(),
                    root -> root.join(Post_.tags, JoinType.LEFT).get(Tags_.id)));
            }
            if (criteria.getCategoryId() != null) {
                specification = specification.or(buildSpecification(criteria.getCategoryId(),
                    root -> root.join(Post_.category, JoinType.LEFT).get(Category_.id)));
            }
            // AND for security reason
            if (criteria.getPublish() != null) {
                specification = specification.and(buildSpecification(criteria.getPublish(), Post_.publish));
            }
        }
        return specification;
    }

    /**
     * Function to convert {@link PostCriteria} to a {@link Specification}
     * Specification : Criteria will be build with MIXED logic operator.
     *
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<Post> createFilterSpecification(PostCriteria criteria) {
        Specification<Post> specification = Specification.where(null);

        if (criteria != null) {

            // return only published post for non USER
            if (!SecurityUtils.isAuthenticated()) {
                BooleanFilter isPublish = new BooleanFilter();
                isPublish.setEquals(true);
                criteria.setPublish(isPublish);
            }

            if (criteria.getId() != null) {
                specification = specification.or(buildSpecification(criteria.getId(), Post_.id));
            }
            if (criteria.getSlug() != null) {
                specification = specification.or(buildStringSpecification(criteria.getSlug(), Post_.slug));
            }
            if (criteria.getTitle() != null) {
                specification = specification.or(buildStringSpecification(criteria.getTitle(), Post_.title));
            }
            if (criteria.getDescription() != null) {
                specification = specification.or(buildStringSpecification(criteria.getDescription(), Post_.description));
            }
            if (criteria.getContent() != null) {
                specification = specification.or(buildStringSpecification(criteria.getContent(), Post_.content));
            }
            // filters needs AND logic
            if (criteria.getTagsId() != null) {
                specification = specification.and(buildSpecification(criteria.getTagsId(),
                    root -> root.join(Post_.tags, JoinType.LEFT).get(Tags_.id)));
            }
            if (criteria.getCategoryId() != null) {
                specification = specification.and(buildSpecification(criteria.getCategoryId(),
                    root -> root.join(Post_.category, JoinType.LEFT).get(Category_.id)));
            }
            // AND for security reason
            if (criteria.getPublish() != null) {
                specification = specification.and(buildSpecification(criteria.getPublish(), Post_.publish));
            }
        }
        return specification;
    }
}
